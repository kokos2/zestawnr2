#include <SFML/Graphics.hpp>
#include <cmath>
#include <cstdint>
#include <iostream>
#include <utility>
#include <array>
#include <numeric>

enum class Dimension : const char
{
    Horizontal,
    Vertical
};

enum class ArithmeticOperation : const char
{
    Add,
    Sub,
    Mul
};

enum class Corner
{
    LU,
    RU,
    LD,
    RD,
    CE
};

sf::Vector2f getOrigin(const sf::Image &im, Corner corner)
{
    sf::Vector2f origin;
    sf::Vector2u size = im.getSize();
    
    switch (corner)
    {
        case Corner::LU:
            origin = sf::Vector2f(0, 0);
            break;
        case Corner::RU:
            origin = sf::Vector2f(static_cast<float>(size.x), 0);
            break;
        case Corner::LD:
            origin = sf::Vector2f(0, static_cast<float>(size.y));
            break;
        case Corner::RD:
            origin = sf::Vector2f(static_cast<float>(size.x), static_cast<float>(size.y));
            break;
        case Corner::CE:
            origin = sf::Vector2f((im.getSize().x)/2, (im.getSize().y)/2);
            break;
    }

    return origin;
}


sf::Image grayScale(const sf::Image & inputImage)
{
    const sf::Vector2u imageSize = inputImage.getSize();
    sf::Image grayImage;
    grayImage.create(imageSize.x,imageSize.y);

    for (int y = 0; y < imageSize.y; ++y)
    {
        for (int x = 0; x < imageSize.x; ++x)
        {
            const sf::Color pixel = inputImage.getPixel(x, y);
            const float grayValue = 
                0.2989f * pixel.r +
                0.5870f * pixel.g +
                0.1140f * pixel.b;

            grayImage.setPixel(x, y, sf::Color(grayValue, grayValue, grayValue));
        }
    }
    
    return grayImage;
}

void moveSprite(sf::Sprite & sprite, float offsetX, float offsetY)
{
    sprite.move(offsetX, -offsetY);
}



sf::Image mirrorReflection(const sf::Image & im, const Dimension & operation)
{
    const sf::Vector2u imageSize = im.getSize();
    sf::Image resultImage;
    resultImage.create(imageSize.x,imageSize.y);

    for(int y = 0; y < imageSize.y; y++)
    {
        for (int x = 0; x < imageSize.x; x++)
        {
            const sf::Color pixel = im.getPixel(x, y);

            switch (operation)
            {
                // mirror 
                /*
                    Sets new pixel cords based on loop iteration
                */
                case Dimension::Horizontal:
                    resultImage.setPixel(imageSize.x - x - 1, y, pixel);
                    break;
                case Dimension::Vertical:
                    resultImage.setPixel(x, imageSize.y - y - 1, pixel);
                    break;
                default:
                    break;
            }
        }
    }

    return resultImage;
}

std::pair <int,int> sizeOfBiggerWindowOutsideSprite(
    int xMul, int yMul, int oldWidth, int oldHeight)
{
    std::pair<int, int> newSize;
    newSize.first  = oldWidth * xMul;
    newSize.second = oldHeight * yMul;
    return newSize;
}

std::pair <int,int> spriteInMiddleOfBiggerWindow(
    int sizeX, int sizeY, int imgWidth, int imgHeight)
{
    std::pair<int ,int> newOffset;
    newOffset.first  = (sizeX - imgWidth ) / 2;
    newOffset.second = (sizeY - imgHeight) / 2;
    return newOffset;
} 

std::pair<int, int> resizeWindowAndMoveSprite(
    int xMul, int yMul, int oldWidth, int oldHeight, sf::Sprite & sprite)
{
    std::pair<int, int> newSize = 
    sizeOfBiggerWindowOutsideSprite(xMul, yMul, oldWidth, oldHeight);

    std::pair<int, int> offset  = 
    spriteInMiddleOfBiggerWindow(newSize.first, newSize.second, oldWidth, oldHeight);
    
    sprite.setPosition(offset.first, offset.second);
    
    return newSize;
}

float angleRad(float angle)
{
    return angle * (M_PI/180.0f);
}



sf::Vector2u windowSizeRotation(const sf::Vector2u size, sf::Vector2f origin, float angle) 
{
    sf::Vector2f p[4];

    for (int i = 0; i < 4; ++i) 
    {
        /*
            x and y defines squares
            we have square of points 0,5f; 0,5f
            each corner have diffrent coordinates
        */ 
        float x = (i % 2 == 0 ? -0.5f : 0.5f) * size.x;
        float y = (i < 2 ? -0.5f : 0.5f) * size.y;
        
        /*
            moves and rotates previously calculated vertices
            https://stackoverflow.com/questions/2259476/rotating-a-point-about-another-point-2d
        */
        p[i] = 
        {
            static_cast<float>(origin.x + cos(angle) * x - sin(angle) * y),
            static_cast<float>(origin.y + sin(angle) * x + cos(angle) * y)
        };
    }

    // Find min and max values
    float minX = std::min({p[0].x, p[1].x, p[2].x, p[3].x});
    float minY = std::min({p[0].y, p[1].y, p[2].y, p[3].y});
    float maxX = std::max({p[0].x, p[1].x, p[2].x, p[3].x});
    float maxY = std::max({p[0].y, p[1].y, p[2].y, p[3].y});

    // Returns size of bounding box for new sprite
    return sf::Vector2u(static_cast<std::uint32_t>(maxX - minX), 
                        static_cast<std::uint32_t>(maxY - minY));
}

sf::Image rotate(const sf::Image &im, sf::Vector2f origin, float angle, sf::Vector2u resultImSize) 
{
    const sf::Vector2u imageSize = im.getSize();
    sf::Image resultImage;
    // Ensure the background is transparent (this shows that rotated img is bigger)
    resultImage.create(resultImSize.x, resultImSize.y, sf::Color::Transparent); 

    // caches values of given angle
    const float cos_a = cos(angle);
    const float sin_a = sin(angle);

    // Offset 
    /*
        Calculates the offset of given picture and result one
        we have to take into acount situation where one or both
        dimensions of the result image are smaller then their
        equivalents in the original one therefore the ternary
        operator, the then branch handles case where original 
        image dimensions are bigger than the result one and the
        else branch handles the opposite case
    */
    const sf::Vector2f offset 
    {
        resultImSize.x > imageSize.x ? 
        (resultImSize.x - imageSize.x) / 2.0f : (imageSize.x - resultImSize.x) / -2.0f,

        resultImSize.y > imageSize.y ? 
        (resultImSize.y - imageSize.y) / 2.0f : (imageSize.y - resultImSize.y) / -2.0f
    };

    for (unsigned y = 0; y < imageSize.y; ++y) {
        for (unsigned x = 0; x < imageSize.x; ++x) {

            // Translate coordinates to origin / current pixel cords
            const float translatedX = x - origin.x;
            const float translatedY = y - origin.y;

            // Apply rotation - same equasion from previous func 
            // calculates position of pixel after rotation
            const float rotatedX = origin.x + cos_a * translatedX - sin_a * translatedY;
            const float rotatedY = origin.y + sin_a * translatedX + cos_a * translatedY;

            // Translate coordinates back with the offset to center / cords pixel after origin
            const int finalX = static_cast<int>(rotatedX + offset.x);
            const int finalY = static_cast<int>(rotatedY + offset.y);

            // Check if within the new image bounds and set pixel
            if (finalX >= 0 && finalX < static_cast<int>(resultImSize.x) &&
                finalY >= 0 && finalY < static_cast<int>(resultImSize.y)) 
            {
                resultImage.setPixel(finalX, finalY, im.getPixel(x, y));
            }
        }
    }
    
    return resultImage;
}

sf::Image arithmetic(
    const sf::Image & inputImageOne, 
    const sf::Image & inputImageTwo,
    const ArithmeticOperation & operation)
{
    
    const sf::Vector2u imageSize = inputImageOne.getSize();
    sf::Image resultImage;
    resultImage.create(imageSize.x,imageSize.y);
    
    for (int y = 0; y < imageSize.y; y++)
    {
        for (int x = 0; x < imageSize.x; x++)
        {
            sf::Color pixel1 = inputImageOne.getPixel(x, y);
            sf::Color pixel2 = inputImageTwo.getPixel(x, y);

            switch (operation)
            {
            case ArithmeticOperation::Add :
                    pixel1.r += pixel2.r;
                    pixel1.g += pixel2.g;
                    pixel1.b += pixel2.b;
                break;
            
            case ArithmeticOperation::Sub :
                    pixel1.r -= pixel2.r;
                    pixel1.g -= pixel2.g;
                    pixel1.b -= pixel2.b;
                break;

            case ArithmeticOperation::Mul :
                    pixel1.r *= pixel2.r;
                    pixel1.g *= pixel2.g;
                    pixel1.b *= pixel2.b;
                break;
            
            default:
                break;
            }

            resultImage.setPixel(x, y, pixel1);
        }
    }

    return resultImage;
}

sf::Image arithmeticCutRange(
    const sf::Image & inputImageOne, 
    const sf::Image & inputImageTwo,
    const ArithmeticOperation & operation)
{
    const sf::Vector2u imageSize = inputImageOne.getSize();
    sf::Image resultImage;
    resultImage.create(imageSize.x,imageSize.y);

    // clamps values between 0, 255 casting cuz need more than 255x255    
    #define OPERATOR(op, c)                                 \
        static_cast<std::uint8_t>(std::max(0, std::min(255, \
        static_cast<std::int32_t>(pixel1.c) op              \
        static_cast<std::int32_t>(pixel2.c)                 \
    )))

    for (int y = 0; y < imageSize.y; y++)
    {
        for (int x = 0; x < imageSize.x; x++)
        {
            const sf::Color pixel1 = inputImageOne.getPixel(x, y);
            const sf::Color pixel2 = inputImageTwo.getPixel(x, y);
            
            switch (operation)
            {
            case ArithmeticOperation::Add :
                resultImage.setPixel(x, y, 
                sf::Color {OPERATOR(+, r), OPERATOR(+, g), OPERATOR(+, b)});

                break;
            case ArithmeticOperation::Mul :
                resultImage.setPixel(x, y, 
                sf::Color {OPERATOR(*, r), OPERATOR(*, g), OPERATOR(*, b)});
                
                break;
            case ArithmeticOperation::Sub :
                resultImage.setPixel(x, y, 
                sf::Color {OPERATOR(-, r), OPERATOR(-, g), OPERATOR(-, b)});
                
                break; 
            default:
                break;
            }
        }
    }  

    return resultImage;
}

sf::Color min_values(const sf::Image & im)
{
    sf::Color ret(255,255,255,255);

    for (int y = 0; y < im.getSize().y; y++) 
    {
        for (int x = 0; x < im.getSize().x; x++) 
        {
            const sf::Color p = im.getPixel(x, y);

            if (p.r < ret.r) { ret.r = p.r; }
            if (p.g < ret.g) { ret.g = p.g; }
            if (p.b < ret.b) { ret.b = p.b; }
        }
    }
    
    return ret;
}

sf::Color max_values(const sf::Image & im)
{
    sf::Color ret(0,0,0,255);

    for (int y = 0; y < im.getSize().y; y++) 
    {
        for (int x = 0; x < im.getSize().x; x++) 
        {
            const sf::Color p = im.getPixel(x, y);

            if (p.r > ret.r) { ret.r = p.r; }
            if (p.g > ret.g) { ret.g = p.g; }
            if (p.b > ret.b) { ret.b = p.b; }
        }
    }
    
    return ret;
}

// files loaded into normalization
/*
    needs to load png/bmp file cuz loading jpg encoding 
    seems to have some kind of artifacts due to compression
*/
sf::Image normalization(const sf::Image & inputImage)
{
    const sf::Vector2u imageSize = inputImage.getSize();
    sf::Image resultImage;
    resultImage.create(imageSize.x, imageSize.y);
    
    sf::Color minValues = min_values(inputImage);
    sf::Color maxValues = max_values(inputImage);

    for (int y = 0; y < imageSize.y; y++) 
    {
        for (int x = 0; x < imageSize.x; x++) 
        {
            const sf::Color p = inputImage.getPixel(x, y);

            sf::Color scaled = sf::Color(0,0,0,255);

            if (maxValues.r != minValues.r) {
                scaled.r = 255 * (p.r - minValues.r) / (maxValues.r - minValues.r);
            }

            if (maxValues.g != minValues.g) {
                scaled.g = 255 * (p.g - minValues.g) / (maxValues.g - minValues.g);
            }

            if (maxValues.b != minValues.b) {
                scaled.b = 255 * (p.b - minValues.b) / (maxValues.b - minValues.b);
            }

            resultImage.setPixel(x, y, sf::Color { scaled.r, scaled.g, scaled.b });
        }
    }

    return resultImage;
}

std::pair<int, int> separateInputMoveValues(
    const std::string & delimiter, 
    const std::string & operation)
{
    std::pair<int, int> separatedInput;
    std::string tempInput = operation;
    size_t delimiterPos = tempInput.find(delimiter);
    std::string xPart = tempInput.substr(0, delimiterPos);
    std::string yPart = tempInput.substr(delimiterPos + delimiter.length());
    separatedInput.first  = std::stoi(xPart);
    separatedInput.second = std::stoi(yPart);

    return separatedInput;
}

std::pair<std::string, int> separateInputOriginAngle(
    const std::string & delimiter, 
    const std::string & operation)
{
    std::pair<std::string, int> separatedInput;
    std::string tempInput = operation;
    size_t delimiterPos = tempInput.find(delimiter);
    std::string originPart = tempInput.substr(0, delimiterPos);
    std::string anglePart = tempInput.substr(delimiterPos + delimiter.length());
    separatedInput.first = originPart;
    separatedInput.second = std::stoi(anglePart);

    return separatedInput;
}

Corner convertToEnum(std::string & corner)
{
    Corner c;
         if(corner == "LD") c = Corner::LD;
    else if(corner == "LU") c = Corner::LU;
    else if(corner == "RD") c = Corner::RD;
    else if(corner == "RU") c = Corner::RU;
    else c = Corner::CE;
    return c; // could return from if's
}

void menu()
{
    const std::string tab = "        ";
    const std::string tabBig = "                     ";
    std::cout << "Usage : ./zestawnr2 <task> <operation/all>" << std::endl;
    std::cout << "Tasks : " << std::endl;
    std::cout << tab << "1<task>" << std::endl;
    std::cout << tab << "flip operations" << std::endl;
    std::cout << tab << "operations : " << std::endl;
    std::cout << tabBig << "90<operation>" << std::endl;
    std::cout << tabBig << "180" << std::endl;
    std::cout << tabBig << "270" << std::endl;
    std::cout << tabBig << "choose angle value : <operation>" << std::endl;
    
    std::cout << tab << "2" << std::endl;
    std::cout << tab << "mirrors" << std::endl;
    std::cout << tabBig << "h <- horizontal" << std::endl;
    std::cout << tabBig << "v <- vertical" << std::endl;
    std::cout << tabBig << "all" << std::endl;

    std::cout << tab << "3" << std::endl;
    std::cout << tab << "3x bigger window" << std::endl;
    std::cout << tabBig << "doesnt matter type anything" << std::endl;

    std::cout << tab << "4" << std::endl;
    std::cout << tab << "move sprite" << std::endl;
    std::cout << tabBig << "type vector : <x,y>" << std::endl;

    std::cout << tab << "5" << std::endl;
    std::cout << tab << "rotate origin" << std::endl;
    std::cout << tabBig << "LU,<angle>" << std::endl;
    std::cout << tabBig << "LD" << std::endl;
    std::cout << tabBig << "RU" << std::endl;
    std::cout << tabBig << "RD" << std::endl;

    std::cout << tab << "6" << std::endl;
    std::cout << tab << "arit, cutRange, normalization" << std::endl;
    std::cout << tabBig << "arits :" << std::endl;
    std::cout << tabBig << "Aadd" << std::endl;
    std::cout << tabBig << "Asub" << std::endl;
    std::cout << tabBig << "Amul" << std::endl << std::endl;
    std::cout << tabBig << "cutRange :" << std::endl;
    std::cout << tabBig << "Cadd" << std::endl;
    std::cout << tabBig << "Csub" << std::endl;
    std::cout << tabBig << "Cmul" << std::endl << std::endl;
    std::cout << tabBig << "normalization :" << std::endl;
    std::cout << tabBig << "Nadd" << std::endl;
    std::cout << tabBig << "Nsub" << std::endl;
    std::cout << tabBig << "Nmul" << std::endl << std::endl;
    std::cout << tabBig << "Mul,Normal same img :" << std::endl;
    std::cout << tabBig << "same" << std::endl << std::endl;
    std::cout << tabBig << "all windows : " << std::endl;
    std::cout << tabBig << "all" << std::endl << std::endl;
}

class MoreWindows {
public:
    MoreWindows() = default;

    MoreWindows(
        const std::string & title, 
        const sf::Sprite & sprite, 
        const sf::Texture & texture, 
        sf::Vector2u size) 
        : savePath(title), sprite(sprite), texture(texture) 
    {
        this->sprite.setTexture(this->texture);
        window.create(sf::VideoMode(size.x, size.y), title, sf::Style::None);
        renderTexture.create(size.x, size.y);
    }

    void screenShot() const 
    {
        renderTexture.getTexture().copyToImage().saveToFile(savePath);
    }

    void run() {

        if (!window.isOpen()) { return; }

        sf::Event event;
        while (window.pollEvent(event)) 
        {
            if (event.type == sf::Event::Closed) { window.close(); }
        }

        renderTexture.clear(sf::Color::Black);
        renderTexture.draw(sprite);
        renderTexture.display();

        window.clear();
        sf::Sprite spr (renderTexture.getTexture());
        window.draw(spr);
        window.display();
    }

    bool isOpen() const {
        return window.isOpen();
    }

private:
    sf::Texture texture;
    const std::string savePath; 
    sf::RenderWindow window;
    sf::RenderTexture renderTexture;
    sf::Sprite sprite;
};

class Application 
{    
    void pushWindow(const std::string & title, const sf::Image & im, sf::Vector2u size) 
    {
        sf::Texture texture;
        texture.loadFromImage(im);
        sf::Sprite sprite;
        sprite.setTexture(texture);
    
        new (&windows[count++]) MoreWindows { title, sprite, texture, size };
    }

    void pushWindow(const std::string & title, const sf::Sprite & sprite, sf::Vector2u size) 
    {
        new (&windows[count++]) MoreWindows { title, sprite, *sprite.getTexture(), size };
    }
public:

    void run(const std::string & task, const std::string & operation)
    {
        sf::Texture txt0, txt1;
        txt0.loadFromFile("../images/szczerbatek.jpg");
        txt1.loadFromFile("../images/jungle.jpg");
 
        int imgHeight = (txt0.getSize().y);
        int imgWidth  = (txt0.getSize().x);
    
        sf::Image im0, im1;
        im0 = txt0.copyToImage();
        im1 = txt1.copyToImage();
        
        sf::Texture resultTexture;
        std::pair<int, int> tempSize = std::make_pair(imgWidth,imgHeight);

        // flip 90 180 270
        if(task == "1")
        {
            const int angle = stoi(operation);

            sf::Vector2u size = 
                windowSizeRotation(
                    im0.getSize(), 
                    sf::Vector2f(static_cast<float>(imgHeight)/2, static_cast<float>(imgWidth)/2), 
                    angleRad(angle)
                );

            sf::Image rotatedImg = rotate(im0, getOrigin(im0, Corner::CE), angleRad(angle), size);
            
            pushWindow("../images/1/rotatedImg.jpg", rotatedImg, size);
        }
        // horizontal, vertical flip
        else if(task == "2")
        {
            sf::Image mirrorHor = mirrorReflection(im0, Dimension::Horizontal);
            sf::Image mirrorVer = mirrorReflection(im0, Dimension::Vertical);
            
            if(operation == "v")   
            {
                pushWindow("../images/2/Vertical.jpg", mirrorVer, mirrorVer.getSize());
            }
            else if(operation == "h") 
            {
                pushWindow("../images/2/Horizontal.jpg", mirrorHor, mirrorHor.getSize());
            }
            else if(operation == "all")
            {
                pushWindow("../images/2/Vertical.jpg", mirrorVer, mirrorVer.getSize());
                pushWindow("../images/2/Horizontal.jpg", mirrorHor, mirrorHor.getSize());
            }
        }
        // 3x bigger window, sprite center
        else if(task == "3")
        {
            sf::Sprite sprite;
            sprite.setTexture(txt0);
            
            const std::pair<int, int> newSize = 
                resizeWindowAndMoveSprite(3, 3, imgWidth, imgHeight, sprite);

            pushWindow("../images/3/biggerWindow.jpg", 
                sprite, sf::Vector2u(newSize.first,newSize.second));
        }
        // move sprite
        else if(task == "4")
        {
            sf::Sprite sprite;
            sprite.setTexture(txt0);

            const int wndMul = 3;
            const std::pair<int, int> newSize = 
                resizeWindowAndMoveSprite(wndMul, wndMul, imgWidth, imgHeight, sprite);

            const std::string delimiter = ",";
            const std::pair<int,int> position = 
                separateInputMoveValues(delimiter, operation);
            
            moveSprite(sprite, position.first, position.second);
            
            pushWindow("../images/4/movedSprite.jpg", 
                sprite, sf::Vector2u(newSize.first,newSize.second));
        }
        // rotate corner
        else if(task == "5")
        {
            const std::string delimiter = ",";
            std::pair<std::string, int> separatedInput = 
                separateInputOriginAngle(delimiter, operation);
            
            int angle = separatedInput.second;
            std::string corner = separatedInput.first;
            Corner c = convertToEnum(corner);

            sf::Vector2f origin = getOrigin(im0, c);
            sf::Vector2u size = windowSizeRotation(im0.getSize(), origin, angleRad(angle));
            sf::Image rotatedImg = rotate(im0, origin, angleRad(angle), size * 3u);

            pushWindow("../images/5/rotatedImg.jpg", rotatedImg, rotatedImg.getSize());
        }
        // arit, color
        else if(task == "6")
        {
            sf::Image arithmeticAdd    = arithmetic(im0, im1, ArithmeticOperation::Add);
            sf::Image arithmeticSub    = arithmetic(im0, im1, ArithmeticOperation::Sub);
            sf::Image arithmeticMul    = arithmetic(im0, im1, ArithmeticOperation::Mul);
            sf::Image cutRangeAdd      = arithmeticCutRange(im0, im1, ArithmeticOperation::Add);
            sf::Image cutRangeSub      = arithmeticCutRange(im0, im1, ArithmeticOperation::Sub);
            sf::Image cutRangeMul      = arithmeticCutRange(im0, im1, ArithmeticOperation::Mul);
            sf::Image normalizationAdd = normalization(arithmeticAdd);
            sf::Image normalizationSub = normalization(arithmeticSub);
            sf::Image normalizationMul = normalization(arithmeticMul);

            //test purposes same image
            sf::Image normalizationSameImMul = 
                normalization(arithmetic(im0,im0, ArithmeticOperation::Mul));

            //min_values(im0); // test of jpeg is wrong
            //max_values(im0);

            if(operation == "Aadd") 
            {
                pushWindow("../images/6/arithmeticAdd.jpg", 
                arithmeticAdd, arithmeticAdd.getSize());
            }

            else if(operation == "Asub") 
            {
                pushWindow("../images/6/arithmeticSub.jpg", 
                arithmeticSub, arithmeticSub.getSize());
            }

            else if(operation == "Amul") 
            {
                pushWindow("../images/6/arithmeticMul.jpg", 
                arithmeticMul, arithmeticMul.getSize());
            }

            else if(operation == "Cadd") 
            {
                pushWindow("../images/6/cutRangeAdd.jpg", 
                cutRangeAdd, cutRangeAdd.getSize());
            }

            else if(operation == "Csub") 
            {
                pushWindow("../images/6/cutRangeSub.jpg", 
                cutRangeSub, cutRangeSub.getSize());
            }

            else if(operation == "Cmul") 
            {
                pushWindow("../images/6/cutRangeMul.jpg", 
                cutRangeMul, cutRangeMul.getSize());
            }

            else if(operation == "Nadd") 
            {
                pushWindow("../images/6/normalizationAdd.jpg", 
                normalizationAdd, normalizationAdd.getSize());
            }

            else if(operation == "Nsub") 
            {
                pushWindow("../images/6/normalizationSub.jpg", 
                normalizationSub, normalizationSub.getSize());
            }

            else if(operation == "Nmul") 
            {
                pushWindow("../images/6/normalizationMul.jpg", 
                normalizationMul, normalizationMul.getSize());
            }

            else if(operation == "same") 
            {
                pushWindow("../images/6/normalizationSameImMul.jpg", 
                normalizationSameImMul, normalizationSameImMul.getSize());
            }

            else if(operation == "all")
            {
                pushWindow("../images/6/arithmeticAdd.jpg", 
                    arithmeticAdd, arithmeticAdd.getSize());
                pushWindow("../images/6/cutRangeAdd.jpg", 
                    cutRangeAdd, cutRangeAdd.getSize());
                pushWindow("../images/6/normalizationAdd.jpg", 
                    normalizationAdd, normalizationAdd.getSize());
                pushWindow("../images/6/arithmeticSub.jpg", 
                    arithmeticSub, arithmeticSub.getSize());
                pushWindow("../images/6/cutRangeSub.jpg", 
                    cutRangeSub, cutRangeSub.getSize());
                pushWindow("../images/6/normalizationSub.jpg", 
                    normalizationSub, normalizationSub.getSize());
                pushWindow("../images/6/arithmeticMul.jpg", 
                    arithmeticMul, arithmeticMul.getSize());
                pushWindow("../images/6/cutRangeMul.jpg", 
                    cutRangeMul, cutRangeMul.getSize());
                pushWindow("../images/6/normalizationMul.jpg", 
                    normalizationMul, normalizationMul.getSize());
                pushWindow("../images/6/normalizationSameImMul.jpg", 
                    normalizationSameImMul, normalizationSameImMul.getSize());
            }
        }
        else
        {
            pushWindow("../images/default.jpg", im0, im0.getSize());
            std::cout << "default image shown" << std::endl;
        }

        while (true) 
        {
            std::size_t open = 0u;
            
            for (std::size_t i = 0u; i < count; i++) 
            {
                if (windows[i].isOpen()) { open++; }
            }

            if (open == 0) {
                break;
            }

            for (std::size_t i = 0u; i < count; i++) {
                windows[i].run();
            }
        }

        for (std::size_t i = 0u; i < count; i++) 
        {
            windows[i].screenShot();
        }
    }

private:
    std::array<MoreWindows, 10> windows = {};
    std::size_t count = 0;
};

int main (int argc, char* argv[])
{
    menu();

    if (argc != 3)
    {
        std::cerr << "Usage: ./zestawnr2 <task> <operation/all>" << std::endl;
        return 1;
    }

    std::string task(argv[1]);
    std::string operation(argv[2]);
    Application app;
    app.run(task, operation);

    return 0;
}

// TODO :
/*
    rotating origin, creates slightly bigger window
*/
